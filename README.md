## Update User Info App
A simple app to update an existing user list using express.js, vue.js, and node.js

## Setup
> Install
```powershell
npm install
```
> Start server
```powershell
node server.js
```
> Run
```powershell
npm run dev
```

## Thanks
Hello to the person reviewing this project!

I want to say thank you for giving me the opportunity to make this app. I've definitely learned a lot of things (as this is my first time setting up a whole vue app from scratch).

I'd like to thank and note a few of my references:

- this awesome article I've come across: [Building a REST API with Node and Express](https://stackabuse.com/building-a-rest-api-with-node-and-express/)
- the great [Paul Underwood](https://github.com/paulund) for his insightful vue CRUD app


A few things to note:

- I've spent a bulk of my time trying to fix the API calls so that the `baseUrl` of the server won't be on the call, but I can't figure this one out yet. It's something that will continue to try to fix after this exam 😁 

Overall, I've had an exciting time buiding this app. I will likely continue to practice and build more stuff like this in the future! So I will leave my github account [here](https://github.com/exiledturtle).