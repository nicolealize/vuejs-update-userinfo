import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource';

import App from './App.vue'

Vue.use(VueRouter)
Vue.use(VueResource);

const ListUsers = require('./js/list-users.vue');
const EditUser = require('./js/edit-user.vue');

const routes = [
    {
        name: 'list-users',
        path: '/',
        component: ListUsers
    },
    {
        name: 'edit-user',
        path: '/user/edit/:id',
        component: EditUser
    }
];

var router = new VueRouter({ routes: routes, mode: 'history' });
new Vue(Vue.util.extend({ router }, App)).$mount('#app');