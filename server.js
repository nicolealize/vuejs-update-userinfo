const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

var USER_DATA = path.join(__dirname, 'src/data/user-data.json');

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.set('port', (process.env.PORT || port));
app.use('/', express.static(__dirname));



app.listen(app.get('port'), function() {
    console.log('Listening on port ' + app.get('port'));
});

app.get('/api/users', function(req, res) {
    fs.readFile(USER_DATA, function(err, data) {
        if (err) {
            console.error(err);
            process.exit(1);
        }
        res.json(JSON.parse(data));
    });
});

app.get('/api/users/:id', function(req, res) {
    fs.readFile(USER_DATA, function(err, data) {
        if (err) {
            console.error(err);
            process.exit(1);
        }

        var users = JSON.parse(data);
        for(var i = 0; i <= users.length; i++)
        {
            if(users[i].id == req.params.id)
            {
                res.json(users[i]);
                break;
            }
        }
    });
});

app.patch('/api/users/edit/:id', function(req, res) {
    fs.readFile(USER_DATA, function(err, data) {
        if (err) {
            console.error(err);
            process.exit(1);
        }

        var users = JSON.parse(data);
        for(var i = 0; i <= users.length; i++)
        {
            if(users[i].id == req.params.id)
            {
                var user = users[i];
                user.name = req.body.name;
                user.address = req.body.address;

                fs.writeFile(USER_DATA, JSON.stringify(users), function(err) {
                    if (err) {
                        console.error(err);
                        process.exit(1);
                    }
                    res.json(users);
                });
                break;
            }
        }
    });
});